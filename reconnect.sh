#! /bin/bash

# Disconnect, then reconnect to ProtonVPN servers

sudo protonvpn-cli --disconnect
if [ $# -gt 0 ]
then
	options="-m"
else
	options="-sc"
fi
sudo protonvpn-cli $options
